### myweb

## Database: postgres

# Tạo env, install thư viện:

```
pip install python3.10
git clone https://gitlab.com/quannn2/project-login-django.git
pip install django
pip install django-auth
```

# Setup database trong file settings.py

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'website_django',
        'USER': 'admin',
        'PASSWORD': '123456',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
```

# Migrate database:

```
cd/googleDjango
python manage.py migrate
```

# Run app:

```
python manage.py runserver
```

# Tạo super user by form django:

```
python manage.py createsuperuser
```

# Tạo user by user,password:

```
Register before login
```

# Tạo user by google:

```
Click login by google
```

# Upload file

```
http://127.0.0.1:8000/upload
```

# List file pdf

```
http://127.0.0.1:8000/list
```

## Name

project-login-django

## Description

Project is form login django

