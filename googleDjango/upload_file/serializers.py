from django.core.validators import FileExtensionValidator
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from .models import FileUpload


class UploadSerializer(ModelSerializer):
    file = serializers.FileField(validators=[FileExtensionValidator(['pdf'])])

    class Meta:
        model = FileUpload
        fields = ['file', 'timestamp']
