from django import forms

from upload_file.models import FileUpload


class DocumentForm(forms.ModelForm):
    class Meta:
        model = FileUpload
        fields = ('file', 'timestamp')
