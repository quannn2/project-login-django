from django.core.validators import FileExtensionValidator
from django.db import models


class FileUpload(models.Model):
    file = models.FileField(null=False, blank=False, validators=[FileExtensionValidator(['pdf'])])
    timestamp = models.DateTimeField(auto_now_add=True)
