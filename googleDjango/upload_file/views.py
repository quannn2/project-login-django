from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .models import FileUpload
from .serializers import UploadSerializer


class FileViewSet(ModelViewSet):
    id = FileUpload.objects.none()
    serializer_class = UploadSerializer
    queryset = FileUpload.objects.all()

    def create(self, request):
        file = request.FILES.get('file')
        content_type = file.content_type
        serializer = UploadSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        response = "You upload successful a file".format(content_type)
        return Response(response)

    def retrieve(self, request, pk=None):
        instance = self.get_object()
        return Response(self.serializer_class(instance).data, status=status.HTTP_200_OK)
