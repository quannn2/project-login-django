from django.urls import path, include
from rest_framework import routers

from .views import FileViewSet

router = routers.DefaultRouter()
router.register(r'files', FileViewSet, basename="upload")

urlpatterns = [
    path('', include(router.urls)),

]
