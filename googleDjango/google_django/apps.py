from django.apps import AppConfig


class GoogledjangoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'google_django'
