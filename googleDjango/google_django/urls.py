from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from google_django import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', views.login_request, name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('social-auth/', include('social_django.urls', namespace='social')),
    path('', views.home, name='home'),
    path('home/', views.home_request, name='home'),
    path("register/", views.register_request, name="register")
]
